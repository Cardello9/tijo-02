import java.util.ArrayList;
import java.util.List;

class LineConverterImpl implements LineConverter {

    /**
     * Implements LineConverter. We have to split lines and
     * trim data from the csv file
     */

    List<Team> teamList= new ArrayList<Team>();

    public List<Team> toTeam(List<String> lines, String splitBy) {
        //System.out.println(lines);
        for(String line: lines) {
            //System.out.println(line);
            String[] teamData = line.split(splitBy);
            //System.out.println(teamData[0]+teamData[1]+ teamData[3]);
            teamData[3] = teamData[3].trim();
            teamData[4] = teamData[4].trim();
            teamData[6] = teamData[6].trim();
            teamData[5] = teamData[5].trim();
            Team team = new Team(Integer.parseInt(teamData[0]), teamData[1], Integer.parseInt(teamData[3]),
                    Integer.parseInt(teamData[4]), Integer.parseInt(teamData[6]), Integer.parseInt(teamData[5]));
            teamList.add(team);
        }
        return teamList;
    }
}
