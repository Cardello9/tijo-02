import java.util.List;

interface LineConverter {
    /**
     * Function returns a list of teams.
     * Arguments are: list of strings and
     * string defining character used to split
     */
    List<Team> toTeam(List<String> lines, String splitBy);
}