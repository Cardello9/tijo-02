import java.util.List;

interface CsvReport {
    /**
     * Function returns a list of Strings
     */
    List<String> csvToListOfString();
}