class Team {

    /**
     * Represents one team. Name is a
     * string, other fields are integer.
     */

    String name;
    Integer id, points, wins, loses, draws;

    Team(Integer id, String name, Integer points, Integer wins, Integer loses, Integer draws) {
        this.id = id;
        this.name = name;
        this.points = points;
        this.wins = wins;
        this.loses = loses;
        this.draws = draws;
    }

    public Integer getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public Integer getPoints() {
        return this.points; }

    public Integer getWins() {
        return this.wins;
    }

    public Integer getLoses() {
        return this.loses;
    }

    public Integer getDraws() {
        return this.draws;
    }

    @Override
    public String toString() {

        /**
         * Allows to represent a team as a string.
         */

        return "ID: " + this.getId().toString() + " NAZWA: " + this.getName() + " PUNKTY: " + this.getPoints().toString()
                + " ZWYCIESTWA: " + this.getWins().toString() + " PORAZKI: " + this.getLoses().toString() + " REMISY:"
                +this.getDraws().toString();
    }
}
