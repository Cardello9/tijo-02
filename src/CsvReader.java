import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class CsvReader implements CsvReport {

    /**
     * This class opens and reads our csv file.
     * Then translates it to a list of string.
     */

    List<String> stringTeamList = new ArrayList<>();
    String csvFile;

    CsvReader(String csvFile)  {
        this.csvFile = csvFile;
    }


    @Override
    public List<String> csvToListOfString() {
        BufferedReader br = null;
        String line = "";
        String CSV_SPLIT_BY = ";";
        Integer lineIndex = 0;

        try {

            br = new BufferedReader(new FileReader(this.csvFile));
            while ((line = br.readLine()) != null) {
                if (lineIndex > 0) {
                    this.stringTeamList.add(line);
                }
                lineIndex += 1;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return this.stringTeamList;
    }
}